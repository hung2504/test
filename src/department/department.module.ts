import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DepartmentController } from './controller/department.controller';
import { Department } from './department.entity';
import { DepartmentRepository } from './repository/department.repository';
import { DepartmentService } from './services/department.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Department, DepartmentRepository]),
  ],
  providers: [DepartmentService],
  controllers: [DepartmentController],
  exports: [DepartmentService]

})
export class DepartmentModule { }
