import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { Department } from '../department/department.entity'

@Entity()
export class Person {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column({ length: 100, nullable: true })
  gender: string;

  @Column()
  age: number;

  @Column({ length: 50 })
  mail: string;

  @ManyToOne(type => Department, department => department.persons)
  department: Department
}
