import { Injectable } from '@nestjs/common';
import { DepartmentRepository } from 'src/department/repository/department.repository';
import { DepartmentService } from 'src/department/services/department.service';
import { getRepository } from 'typeorm';
import { AddManagerDepart, PersonDTO, Statistics } from '../dto/create-person.dto';
import { Person } from '../person.entity';
import { PersonRepository } from '../repository/person.repository';

@Injectable()
export class PersonService {
  constructor(
    private departmentService: DepartmentService,
    private PersonRepo: PersonRepository
  ) { }
  findAll() {
    return this.PersonRepo.find();
  }
  findOne(id: number) {
    return this.PersonRepo.findOne(id);
  }

  async create(body: PersonDTO) {
    const newUser = new Person();
    newUser.name = body.name;
    newUser.age = body.age;
    newUser.gender = body.gender;
    newUser.mail = body.mail;
    const department = await this.departmentService.findOne(body.departmentId)
    console.log(department)
    newUser.department = department
    return this.PersonRepo.save(newUser);
  }

  async add(body: Person) {
    return this.PersonRepo.save(body);
  }


  async addManageDepartment(addManager: AddManagerDepart) {
    const department = await this.departmentService.findOne(addManager.departmentId)
    department.managerDepart = await this.findOne(addManager.personId)
    return this.departmentService.updateDepartment(department)
  }

  async addPersonToDepartment(addManager: AddManagerDepart) {
    const department = await this.departmentService.findOne(addManager.departmentId)
    const user = await this.findOne(addManager.personId);
    user.department = department
    return this.PersonRepo.update(user.id, user)
  }

  async getStatistic() {
    const departments = await this.departmentService.getAllDepartment();
    const listStatisticals = []

    for (const department of departments) {
      console.log(department);
      let females = await this.getCountGender(department.id, "female");
      let males = await this.getCountGender(department.id, "male");
      let total = await this.getTotalPerson(department.id)
      console.log('managerId:' + department.managerDepartId)
      let manager = await this.findOne(department.managerDepartId)
      let statistical = new Statistics()
      if (department.managerDepartId === null) {

        statistical.departId = department.id
        statistical.departName = department.name
        statistical.female = females
        statistical.male = males
        statistical.totalPerson = total
        statistical.managerName = null
      } else {

        statistical.departId = department.id
        statistical.departName = department.name
        statistical.female = females
        statistical.male = males
        statistical.totalPerson = total
        statistical.managerName = manager.name
      }
      listStatisticals.push(statistical)
    }
    return listStatisticals
  }

  async getStatisticalOneDepartment(id: number) {
    // const department = await this.departmentService.getDepartById(id);
    const department = await this.departmentService.findOne(id);
    if (department === undefined) {

      console.log("loi");

    } else {
      console.log(department)
      let females = await this.getCountGender(department.id, "female");
      let males = await this.getCountGender(department.id, "male");
      let total = await this.getTotalPerson(department.id)
      //   let manager = await this.findOne(department.managerDepart.id)
      //   if (manager === undefined) {
      //     console.log("loi2");
      //  }
      let manager = null
      const departments = await this.departmentService.getAllDepartment();
      for (const department1 of departments) {
        if (department1.id === department.id) {
          manager = await this.findOne(department1.managerDepartId)
        }
      }
      let statistical = new Statistics()
      statistical.departId = department.id
      statistical.departName = department.name
      statistical.female = females
      statistical.male = males
      statistical.totalPerson = total
      statistical.managerName = manager.name
      return statistical
    }

  }

  async getCountGender(idDepartment: number, genderI: string) {
    return await getRepository(Person)
      .createQueryBuilder("person")
      .select("person.id")
      .where("person.departmentId = :id", { id: idDepartment })
      .andWhere("person.gender = :gender", { gender: genderI })
      .getCount()
  }

  async getTotalPerson(idDepartment: number) {
    return await getRepository(Person)
      .createQueryBuilder("person")
      .select("person.id")
      .where("person.departmentId = :id", { id: idDepartment })
      .getCount()
  }
}
