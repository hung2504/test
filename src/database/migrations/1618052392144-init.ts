import {MigrationInterface, QueryRunner} from "typeorm";

export class init1618052392144 implements MigrationInterface {
    name = 'init1618052392144'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "person" ("id" SERIAL NOT NULL, "name" character varying(50) NOT NULL, "gender" character varying(100), "age" integer NOT NULL, "mail" character varying(50) NOT NULL, "departmentId" integer, CONSTRAINT "PK_5fdaf670315c4b7e70cce85daa3" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "department" ("id" SERIAL NOT NULL, "name" character varying(50) NOT NULL, "description" character varying(500) NOT NULL, "managerDepartId" integer, CONSTRAINT "REL_781fe90f3eee8989b526ec4e45" UNIQUE ("managerDepartId"), CONSTRAINT "PK_9a2213262c1593bffb581e382f5" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "person" ADD CONSTRAINT "FK_5ad0e94b674f9ffc1200893a479" FOREIGN KEY ("departmentId") REFERENCES "department"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "department" ADD CONSTRAINT "FK_781fe90f3eee8989b526ec4e459" FOREIGN KEY ("managerDepartId") REFERENCES "person"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "department" DROP CONSTRAINT "FK_781fe90f3eee8989b526ec4e459"`);
        await queryRunner.query(`ALTER TABLE "person" DROP CONSTRAINT "FK_5ad0e94b674f9ffc1200893a479"`);
        await queryRunner.query(`DROP TABLE "department"`);
        await queryRunner.query(`DROP TABLE "person"`);
    }

}
