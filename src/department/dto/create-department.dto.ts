export class DepartmentDTO {
    readonly name: string;
    readonly description: string;
}