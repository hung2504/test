export class PersonDTO {
  readonly name: string;
  readonly gender: string;
  readonly age: number;
  readonly mail: string;
  readonly departmentId: number;
}
//Thêm quản lý
export class AddManagerDepart {
  readonly departmentId: number;
  readonly personId: number;
}
//Thống kê
export class Statistics {
  departId: number;
  departName: string;
  female: number;
  male: number;
  totalPerson: number;
  managerName: string;
}
