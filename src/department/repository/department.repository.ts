import { EntityRepository, Repository } from 'typeorm';
import { Department } from '../department.entity';
import { DepartmentDTO } from '../dto/create-department.dto';

@EntityRepository(Department)
export class DepartmentRepository extends Repository<Department>{
  createDepartment = async (departmentDto: DepartmentDTO) => {
    return await this.save(departmentDto)
  }

  findOneDepartment = async (id: string) => {
    return this.findOneOrFail(id)
  }
}